use std::error::Error;
use std::path::Path;

use log;

use config::Config;

mod cli;
mod config;
mod db;
mod init_log;
mod prelude;
mod wa;

pub async fn main() -> Result<(), Box<dyn Error>> {
    let arguments = cli::get_arguments();
    let path = arguments.value_of("config").map(|path| Path::new(path));
    let config = config::load_config(path).await?;
    init_log::init_logger(&config);
    let pool = db::init::get_db(&config).await?;
    db::init::init_db(&pool).await?;
    let whatsapp = wa::handler(&config, &pool);
    futures::try_join!(whatsapp)?;
    Ok(())
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
