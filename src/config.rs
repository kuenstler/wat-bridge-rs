use std::path::Path;

use confy;
use serde_derive::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
#[serde(default)]
pub struct Config<'a> {
    pub loglevel: u32,
    // 0 means nothing, scales up to 4, see init_log for more information
    pub logfile: Option<String>,
    pub database_uri: String,
    pub whatsapp_conn: Option<whatsappweb_eta::session::PersistentSession>,
    #[serde(skip)]
    pub path: Option<&'a Path>,
}

impl<'a> Default for Config<'a> {
    fn default() -> Self {
        Self {
            loglevel: 1,
            logfile: None,
            database_uri: "postgres://localhost".to_string(),
            whatsapp_conn: None,
            path: None,
        }
    }
}

pub async fn load_config(use_path: Option<&Path>) -> Result<Config<'_>, confy::ConfyError> {
    match &use_path {
        Some(path) => confy::load_path(path),
        None => {
            let config = confy::load("wat-bridge")?;
            confy::store("wat-bridge", &config)?;
            Ok(config)
        }
    }.map(|mut config: Config| {
        config.path = use_path;
        config
    })
}

pub async fn save_config(config: &&Config<'_>) -> Result<(), confy::ConfyError> {
    match &config.path {
        Some(path) => confy::store_path(path, config),
        None => confy::store("wat-bridge", config),
    }
}
