use log;

#[async_std::main]
async fn main() {
    match wat_bridge::main().await {
        Ok(_) => (),
        Err(err) => log::error!("{:?}", err),
    };
}
