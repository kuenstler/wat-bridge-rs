use clap::{App, Arg, ArgMatches};

pub fn get_arguments() -> ArgMatches<'static> {
    App::new("wat-bridge")
        .version("0.0.1")
        .author("Fabian Wunsch")
        .about("Connects Whatsapp and Telegram users")
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Set another file than the default os one")
                .takes_value(true),
        )
        .get_matches()
}
