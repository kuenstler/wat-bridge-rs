use futures::compat::Compat01As03;
use whatsappweb_eta;
use whatsappweb_eta::conn::WebConnection;

pub use wa_error::WaError;

use crate::prelude::*;

mod wa_error;

pub async fn handler(config: &Config<'_>, pool: &PgPool) -> Result<(), WaError> {
    log::info!("Logging into whatsapp web");
    let conn = match &config.whatsapp_conn {
        Some(p) => Compat01As03::new(WebConnection::connect_persistent(p.clone())).await,
        None => Compat01As03::new(WebConnection::connect_new()).await,
    }?;
    log::info!("Successfully logged into whatsapp web");
    Ok(())
}
