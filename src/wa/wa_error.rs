use std::error::Error;
use std::fmt;

use whatsappweb_eta::errors;

pub struct WaError {
    internal_error: whatsappweb_eta::errors::WaError,
}

impl fmt::Debug for WaError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&self.internal_error, f)
    }
}

impl fmt::Display for WaError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&self.internal_error, f)
    }
}

impl Error for WaError {}

impl From<errors::WaError> for WaError {
    fn from(err: errors::WaError) -> Self {
        Self {
            internal_error: err,
        }
    }
}
