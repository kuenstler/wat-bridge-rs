use log4rs::append::console::ConsoleAppender;
use log4rs::append::file::FileAppender;
use log4rs::config::{Appender, Config, Root};
use log4rs::encode::pattern::PatternEncoder;
use log::LevelFilter;

pub fn init_logger(config: &super::Config) {
    let pattern = Box::new(PatternEncoder::new(
        "{d(%Y-%m-%dT%H:%M:%S%:z)} [{l}] ((M)) - {m}\n",
    ));
    let appender = match &config.logfile {
        Some(file) => Appender::builder().build(
            "log_location",
            Box::new(
                FileAppender::builder()
                    .encoder(pattern)
                    .build(file)
                    .expect(&format!("Could not write to log file {}", file)),
            ),
        ),
        None => Appender::builder().build(
            "log_location",
            Box::new(ConsoleAppender::builder().encoder(pattern).build()),
        ),
    };

    let loglevel = match config.loglevel {
        0 => LevelFilter::Off,
        1 => LevelFilter::Error,
        2 => LevelFilter::Warn,
        3 => LevelFilter::Info,
        4 | _ => LevelFilter::Debug,
    };

    let config = Config::builder()
        .appender(appender)
        .build(Root::builder().appender("log_location").build(loglevel))
        .unwrap();

    log4rs::init_config(config).unwrap();

    log::info!("Initialized logger");
}
