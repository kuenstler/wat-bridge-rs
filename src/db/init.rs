use futures;

use crate::prelude::*;

pub async fn get_db(config: &Config<'_>) -> Result<PgPool, sqlx::Error> {
    log::info!("Connecting to database");
    log::debug!("With uri {}", &config.database_uri);
    let result = PgPool::connect(&config.database_uri).await;
    log::info!("Successfully connected to database");
    result
}

pub async fn init_db(pool: &PgPool) -> Result<(), sqlx::Error> {
    log::info!("Creating tables if needed");
    let messages = sqlx::query!(
        "
    CREATE TABLE IF NOT EXISTS messages (
        origin_id           varchar(40) PRIMARY KEY,-- May be wa or tg msg id
        sender_id           varchar(40) NOT NULL,   -- The uid of the sender
        context             varchar(40) NOT NULL,   -- Either wa group, tg group or tg bot
        text                text NOT NULL,          -- The actual content
        target_id           int,                    -- May be tg or wa msg id
        attachment          bool,                   -- If the message has an attachment
        reply_to            int,                    -- origin_id of reply message
        delivered           bool NOT NULL DEFAULT false-- If the message got delivered
    );
    "
    )
        .fetch_all(pool);
    let attachments = sqlx::query!(
    "
    CREATE TABLE IF NOT EXISTS attachments (
        id                  varchar(40) PRIMARY KEY,-- The attachment id
        origin_id           varchar(40) NOT NULL UNIQUE REFERENCES messages ON DELETE RESTRICT,-- The sender
        name                text NOT NULL,          -- The filename including extension
        location            text NOT NULL          -- The location where the file is located
    );
    "
    )
        .fetch_all(pool);
    let users_groups = sqlx::query!(
        "
    CREATE TABLE IF NOT EXISTS users_groups (
        id                  varchar(40) PRIMARY KEY,-- The id of the channel or group
        is_user             bool NOT NULL,          -- If the entry is a user
        is_tg               bool NOT NULL,          -- If it is a tg user/group
        name                text NOT NULL,          -- The human readable name
        picture_path        text,                   -- Path of a profile picture
        tg_bot              varchar(40),            -- Id of a tg_bot for wa numbers
        description         text,                   -- The description/bio of a group/user
        members             varchar(40)[],          -- id's of members for a group
        token               char(45),               -- Tg bot token
        password            bytea                   -- Password for a wa user to log on
    );
    "
    )
        .fetch_all(pool);
    let botfather = sqlx::query!(
        "
    CREATE TABLE IF NOT EXISTS botfather (
        queue               varchar(40)[],          -- queue of bots needed to get created
        current             varchar(40),            -- wa id of the current bot
        id                  varchar(40),            -- tg id of the current bot
        state               int                     -- state of the creation of the current bot
    );
    "
    )
        .fetch_all(pool);
    futures::try_join!(messages, attachments, users_groups, botfather)?;
    log::info!("Successfully created tables (if needed)");
    Ok(())
}
